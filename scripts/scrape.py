"""
Use the GitLab Milestones API to get a list of active milestones.

Use this information to understand cutoff dates.
"""
from datetime import datetime, timedelta
from calendar import monthcalendar
import json
import requests

def third_thursday_of_month(month, year):
    """
    Calculate the third thursday of a month given the month and year

    Args:
        month (int): month to look for
        year (int): year to look for

    Returns:
        str: the 3rd Thursday of the month in YYYY-mm-dd
    """
    calendar = monthcalendar(year, month)

    # Simple way to calculate the 3rd Thursday
    if int(calendar[3][3]) < 22:
        third_thursday = calendar[3][3]
    else:
        third_thursday = calendar[2][3]

    date_object = datetime(year, month, third_thursday)
    # return the date in YYYY-mm-dd format
    return date_object.strftime("%Y-%m-%d")

def get_private_token():
    """Read a GitLab PAT from a file. Needs read_api scope

    Returns:
        str: the token
    """
    with open("gitlab-token.txt", encoding='utf-8') as f:
        private_token = f.read().strip()
    return private_token


def get_milestones():
    """Use the API to get a list of active milestones.

    Returns:
        list: list of milestones in JSON format
    """

    # Set headers
    headers = {"Private-Token": get_private_token()}

    # Set params
    params = {"state": "active"}

    # GitLab.org Group
    group_id = 9970

    # Make API request to get milestones
    # This will have multiple pages, so keep requesting until there are no more
    page = 1
    api_url = f"https://gitlab.com/api/v4/groups/{group_id}/milestones"
    milestones = []
    while True:
        response = requests.get(
            api_url, headers=headers, params={**params, "page": page}, timeout=30
        )

        milestones.extend(response.json())

        if not response.links.get("next"):
            break

        page += 1

    return milestones


def sort_milestones(milestones):
    """Sorts the milestones by start date

    Args:
        milestones (list): unsorted list of milestones

    Returns:
        list: sorted list of milestones
    """

    # Filter the list of milestones to ones that have a start date
    filtered_milestones = [
        m for m in milestones if m.get("start_date") and m["start_date"] != "None"
    ]

    filtered_milestones.sort(key=lambda m: m["start_date"])
    return filtered_milestones

def find_quarter(due_date):
    year_string, month, day = due_date.split("-")
    short_year = int(year_string[2:])
    year = short_year if int(month) == 1 else short_year + 1
    return {
        "01": "FY{0}-Q4",
        "02": "FY{0}-Q1",
        "03": "FY{0}-Q1",
        "04": "FY{0}-Q1",
        "05": "FY{0}-Q2",
        "06": "FY{0}-Q2",
        "07": "FY{0}-Q2",
        "08": "FY{0}-Q3",
        "09": "FY{0}-Q3",
        "10": "FY{0}-Q3",
        "11": "FY{0}-Q4",
        "12": "FY{0}-Q4"
    }.get(month).format(year)

def find_milestone_dates():
    """
    Gets the list of milestones from the API
    Output the results to a JS file that can be consumed by the website
    """
    milestones = get_milestones()
    sorted_milestones = sort_milestones(milestones)

    filtered_list = []

    for milestone in sorted_milestones:
        title = milestone["title"]
        start_date = milestone["start_date"]
        due_date = milestone["due_date"]

        if due_date > datetime.now().strftime("%Y-%m-%d") and not title.startswith('Git'):
            due_date_obj = datetime.strptime(due_date, "%Y-%m-%d")
            month = due_date_obj.month
            year = due_date_obj.year
            third_thursday = third_thursday_of_month(month, year)

            # add to filtered list
            filtered_list.append({
                "title": title, 
                "start_date": start_date, 
                "due_date": due_date, 
                "quarter": find_quarter(due_date), 
                "third_thursday": third_thursday})

    print(f"Found {len(filtered_list)} milestones")
    return filtered_list

def write_milestone_dates(dates):
    """Write the filtered_list to a Javascript file with JSON"""
    with open('public/milestones.js', 'w', encoding='utf-8') as f:
        f.write('const milestones = ' + json.dumps(dates))

# Python main functions
def main():
    dates = find_milestone_dates()
    write_milestone_dates(dates)

if __name__ == "__main__":
    main()
