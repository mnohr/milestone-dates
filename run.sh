#!/bin/bash

# Setup environment
if [ ! -d "virtualenv" ]; then
  echo "Settup up virutal environment"
  python3 -m venv virtualenv
  source virtualenv/bin/activate
  virtualenv/bin/pip install -r requirements.txt 
else
  source virtualenv/bin/activate
fi

# Make sure the path is set correctly
export PYTHONPATH=$PYTHONPATH:$(pwd)

# Run the script
virtualenv/bin/python3 scripts/scrape.py