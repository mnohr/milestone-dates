Simple site that lists the upcoming GitLab milestones.

Deployed: https://mnohr.gitlab.io/milestone-dates/

### How it Works

Every week a scheduled CI job runs

This calls `scripts/scrape.py`. This python script calls the GitLab API to get a list of milestones for the GitLab.org group. I filter out the milestones to only return the current and future milestones. This outputs JSON, but in a JavaScript file in the public directory.

This is then deployed with GitLab pages. The output JSON and a static HTML page are deployed.

### Running Locally

You will need to create a `gitalb-token.txt` file that has a GitLab personal access token with Read API permissions.

You will need the `requests` python library, which is listed in `requirements.txt`. 

Then just run `python scripts/scrape.py`